package model.data_structures;

import java.util.Iterator;

public class IterSurf implements Iterator<Nodo>{
	
	private Nodo cabeza;
	
	private Nodo actual;
	
	public IterSurf(Nodo head)
	{
		cabeza=head;
		actual=head;
	}
	public boolean hasNext() {
		return actual.darSiguiente().equals(null)?false:true;
	}
	public Nodo next() {
		// TODO Auto-generated method stub
		Nodo sig=actual.darSiguiente();
		actual=actual.darSiguiente();
		return sig;
	}
}