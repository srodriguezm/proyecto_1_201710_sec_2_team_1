package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private Nodo<T> primer;

	private Nodo<T> ultimo;

	private Nodo<T> actual;

	private int numNodos;

	public ListaEncadenada()
	{

	}

	
	public Iterator<T> iterator() {
		Iterator<T> it= (Iterator<T>) new IterSurf(primer);
		return (Iterator<T>) it;
	}

	
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		if(primer==null)
		{
			primer =new Nodo(elem);
			ultimo=primer;
			actual=primer;
			numNodos++;
		}
		else
		{
			Nodo<T> nuevo=new Nodo(elem);
			ultimo.cambiarSiguiente(nuevo);
			ultimo=nuevo;
			actual=nuevo;
			numNodos++;
		}
	}
	public void agregarElementoPrincipio(T elem)
	{
		Nodo<T> nuevo=new Nodo(elem);
		nuevo.cambiarSiguiente(primer);
		primer=nuevo;
		numNodos++;
	}
	
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if(primer!=null)
		{
		int i=0;
		Nodo actual=primer;
		while(i<pos)
		{
			actual=actual.darSiguiente();
			i++;
		}
		return (T) actual.darElemento();
		}
		return null;
	}


	
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return numNodos;
	}

	
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		if(actual!=null)
		{
			return actual.darElemento();
		}
		return null;
	}

	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if(actual.darSiguiente()!=null)
		{
			actual=actual.darSiguiente();
			return true;
		}
		else
		{
			return false;
		}

	}

	
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(primer!=null&&primer.darSiguiente()!=null)
		{
			Nodo act=primer;
			while(!(act.darSiguiente().equals(actual)))
			{
				act=act.darSiguiente();
			}
			actual=act;
			return true;
		}
		else
		{
			return false;
		}
	}
	
	public void eliminarPrimero()
	{
		Nodo<T> nod=primer.darSiguiente();
		primer=null;
		primer=nod;
		numNodos--;
	}
	public void eliminarUltimo()
	{
		ultimo=null;
		numNodos--;
	}
	public void insertarPos(T elem,int pos)
	{
		int i=0;
		Nodo<T> nuevo=new Nodo<T>(elem);
		Nodo<T> act=primer;
		Nodo<T> ant=null;
		while(i<pos)
		{
			ant=act;
			act=act.darSiguiente();
			i++;
		}
		nuevo.cambiarSiguiente(act);
		ant.cambiarSiguiente(nuevo);
		numNodos++;
	}
	public void eliminarPos(int n)
	{
		int i=0;
		Nodo<T> act=primer;
		Nodo<T> ant=null;
		while(i<n)
		{
			ant=act;
			act=act.darSiguiente();
			i++;
		}
		ant.cambiarSiguiente(act.darSiguiente());
		numNodos--;
	}
	public void swap(int i,int j)
	{
		insertarPos(darElemento(i), j);
		insertarPos(darElemento(j), i);
		eliminarPos(i+1);
		eliminarPos(j+1);
	}

	public boolean existeElemento(T elem)
	{
		boolean hay=false;
		actual=primer;
	while(!hay&&actual!=null)
	{
		if(actual.darElemento().equals(elem))	
			hay=true;
		else
			actual=actual.darSiguiente();
			
	}
		return hay;
	}

}
