package model.data_structures;

public class Queue<T> {

	private ListaEncadenada<T> cola;

	public Queue()
	{
		cola=new ListaEncadenada<T>();
	}
	public void enqueue(T item)
	{
		cola.agregarElementoFinal(item);
	}
	public T dequeue()
	{
		T elem=cola.darElemento(0);
		cola.eliminarPrimero();
		return elem;
	}
	public boolean isEmpty()
	{
		if(cola.darNumeroElementos()==0)
		{
			return true;
		}
		else
		{
			return false;
		}
	}
	public int size()
	{
		return cola.darNumeroElementos();
	}
	public T peek()
	{
		return cola.darElemento(0);
	}

}
