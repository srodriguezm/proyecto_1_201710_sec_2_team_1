package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Iterator;
import model.data_structures.*;

import model.data_structures.ILista;
import model.data_structures.ListaEncadenada;
import model.vo.VOGeneroPelicula;
import model.vo.VOGeneroTag;
import model.vo.VOGeneroUsuario;
import model.vo.VOOperacion;
import model.vo.VOPelicula;
import model.vo.VOPeliculaPelicula;
import model.vo.VOPeliculaUsuario;
import model.vo.VORating;
import model.vo.VOTag;
import model.vo.VOUsuario;
import model.vo.VOUsuarioConteo;
import model.vo.VOUsuarioGenero;
import api.ISistemaRecomendacionPeliculas;
import model.data_structures.*;

public class SistemaRecomendacionPeliculas implements ISistemaRecomendacionPeliculas{
	
	private ListaEncadenada<VOPelicula> pelis;
	
	private ListaEncadenada<VOGeneroPelicula> ogeneros;
	
	private ListaEncadenada<VORating> rateadas;
	
	private ListaEncadenada<VOTag> tageadas;
	
	private ListaEncadenada<VOPelicula> anios;
	
	private ListaEncadenada<String> nomGeneros;
	
	private ListaEncadenada<VOGeneroUsuario> usuariosNumRatingsGenero;
	
	private ListaEncadenada<VOUsuario> usuarios;
	
	private Stack<VOOperacion> operaciones;
		
	public boolean cargarPeliculasSR(String rutaPeliculas) {
		// TODO Auto-generated method stub
		boolean aha=false;
		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaPeliculas)))){
			String linea=in.readLine();
			while(linea!=null)
			{
				String args[]=linea.split(",");
				String[] generos;
				int anio=0;
				String titulo="";
				generos = args[args.length -1].split("\\|");
				ListaEncadenada<String> gen = new ListaEncadenada<String>();
				for (int i = 0; i < generos.length; i++)
				{
					gen.agregarElementoFinal(generos[i]);
					if(!nomGeneros.existeElemento(generos[i]))
					{
						nomGeneros.agregarElementoFinal(generos[i]);
					}
				}
				if(args.length==3)
				{
					String dtitle=args[1].trim();
					String[] ano=dtitle.split("\\)");
					anio = Integer.parseInt(ano[0].substring(ano[0].length() -4, ano[0].length()));
					String tit[]=dtitle.split(String.valueOf(anio));
					titulo=(tit[0].substring(0,tit[0].length()-1));
				}
				else
				{
					String[] comillas=linea.split("\"");
					String []form=comillas[1].split("\\(");
					titulo=form[0];
					try
					{
						anio= Integer.parseInt(comillas[1].substring(comillas[1].length() -6, comillas[1].length() -2));
					}
					catch(NumberFormatException e)
					{
						anio=-1;
					}
					generos=comillas[2].split("\\|");
				}
				
				VOPelicula voPel = new VOPelicula();
				voPel.setAgnoPublicacion(anio);
				voPel.setGenerosAsociados(gen);
				voPel.setTitulo(titulo);

				for (int i = 0; i < gen.darNumeroElementos(); i++) 
				{
					VOGeneroPelicula a= new VOGeneroPelicula();
					a.setGenero(gen.darElemento(i));
					ListaEncadenada<VOPelicula> esta= new ListaEncadenada();
					a.setPeliculas(esta);
					if(!ogeneros.existeElemento(a))
					{
						esta.agregarElementoFinal(voPel);
						a.setPeliculas(esta);
						ogeneros.agregarElementoFinal(a);
					}
					else
						ogeneros.darElementoPosicionActual().agregarPelicula(voPel);
				}
				
				pelis.agregarElementoFinal(voPel);

				linea = in.readLine();
			}
			aha=true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aha;

	}

	public boolean cargarRatingsSR(String rutaRatings) {
		// TODO Auto-generated method stub
		boolean aha=false;
		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaRatings)))){
			String linea=in.readLine();
			while(linea!=null)
			{
				String args[]=linea.split(",");
				int userID=Integer.parseInt(args[0]);
				long movieID=Long.parseLong(args[1]);
				double rating=Double.parseDouble(args[2]);
				long timeStamp=Long.parseLong(args[3]);			

				
				VORating voR = new VORating();

				voR.setIdPelicula(movieID);
				voR.setIdUsuario(userID);
				voR.setRating(rating);
				voR.setTime(timeStamp);

				rateadas.agregarElementoFinal(voR);

				boolean encontrada=false;
				Iterator<VOPelicula> pato=pelis.iterator();
				while(pato.hasNext()&&!encontrada)
				{
					VOPelicula madreMia= pato.next();
					if(madreMia.getIdUsuario()==movieID)
					{
						madreMia.setNumeroRatings(madreMia.getNumeroRatings()+1);
						madreMia.setPromedioRatings(((madreMia.getPromedioRatings()*madreMia.getNumeroRatings())+rating)/(madreMia.getNumeroRatings()+1));
						encontrada=true;
					}
				}
				
				linea = in.readLine();
			}
			aha=true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aha;
	}

	public void ordenarPelisPorRating(ListaEncadenada<VOPelicula> p,int desde, int hasta)
	{
		if(p.darNumeroElementos()<=1)
		{
			int pivote=desde;
			int left=desde+1;
			int rigth=hasta;
			while(left<rigth)
			{
				while(p.darElemento(pivote).getNumeroRatings()<=(p.darElemento(left)).getNumeroRatings())
				{
					left++;
				}
				while(p.darElemento(pivote).getNumeroRatings()>=(p.darElemento(rigth)).getNumeroRatings())
				{
					rigth--;
				}
				if(left<rigth)
				{	
				p.swap(left,rigth);
				}
			}
			p.swap(pivote,left-1);
			ordenarPelisPorRating(p, desde, pivote-1);
			ordenarPelisPorRating(p, pivote+1, hasta);
		}
	}
	public void ordenarPelisCalificacion(ListaEncadenada<VOPelicula> p, int desde, int hasta)
	{
		if(p.darNumeroElementos()<=1)
		{
			int pivote=desde;
			int left=desde+1;
			int rigth=hasta;
			while(left<rigth)
			{
				while(p.darElemento(pivote).getNumeroTags()<=(p.darElemento(left)).getNumeroTags())
				{
					left++;
				}
				while(p.darElemento(pivote).getNumeroTags()>=(p.darElemento(rigth)).getNumeroTags())
				{
					rigth--;
				}
				if(left<rigth)
				{	
				p.swap(left,rigth);
				}
			}
			p.swap(pivote,left-1);
			ordenarPelisCalificacion(p, desde, pivote-1);
			ordenarPelisCalificacion(p, pivote+1, hasta);
		}
	}
	public void ordenarPelisPorTags(ListaEncadenada<VOPelicula> p,int desde,int hasta)
	{
		if(p.darNumeroElementos()<=1)
		{
			int pivote=desde;
			int left=desde+1;
			int rigth=hasta;
			while(left<rigth)
			{
				while(p.darElemento(pivote).getNumeroTags()<=(p.darElemento(left)).getNumeroTags())
				{
					left++;
				}
				while(p.darElemento(pivote).getNumeroTags()>=(p.darElemento(rigth)).getNumeroTags())
				{
					rigth--;
				}
				if(left<rigth)
				{	
				p.swap(left,rigth);
				}
			}
			p.swap(pivote,left-1);
			ordenarPelisCalificacion(p, desde, pivote-1);
			ordenarPelisCalificacion(p, pivote+1, hasta);
		}
	}
	public void ordenarPelisPorDiferCalif(ListaEncadenada<VOPelicula> p,int desde,int hasta, double calif)
	{
		if(p.darNumeroElementos()<=1)
		{
			int pivote=desde;
			int left=desde+1;
			int rigth=hasta;
			while(left<rigth)
			{
				double difpivote=p.darElemento(pivote).getPromedioRatings()-calif;
				if(difpivote<0)
					difpivote*=-1;
				double difleft=p.darElemento(left).getPromedioRatings()-calif;
				if(difleft<0)
					difleft*=-1;
				double difright=p.darElemento(rigth).getPromedioRatings()-calif;
				if(difright<0)
					difright*=-1;
				while(difpivote<=difleft)
				{
					left++;
				}
				while(difpivote>=difright)
				{
					rigth--;
				}
				if(left<rigth)
				{	
				p.swap(left,rigth);
				}
			}
			p.swap(pivote,left-1);
			ordenarPelisPorDiferCalif(p, desde, pivote-1,calif);
			ordenarPelisPorDiferCalif(p, pivote+1, hasta,calif);
		}
	}
	public boolean cargarTagsSR(String rutaTags) {
		// TODO Auto-generated method stub
		boolean aha=false;
		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaTags)))){
			String linea=in.readLine();
			while(linea!=null)
			{
				String args[]=linea.split(",");
				int userID=Integer.parseInt(args[0]);
				long movieID=Integer.parseInt(args[1]);
				String tag=args[2];
				long timeStamp=Integer.parseInt(args[3]);			
				
				Iterator<VOUsuario>iter=usuarios.iterator();
				boolean yaEsta=false;
				while(iter.hasNext())
				{
					VOUsuario act=iter.next();
					if(act.getIdUsuario()==userID)
					{
						yaEsta=true;
					}
				}
				
				if(yaEsta==false)
				{
					VOUsuario user=new VOUsuario();
					user.setIdUsuario(userID);
					if(user.getPrimerTimestamp()>timeStamp)
					{
						user.setPrimerTimestamp(timeStamp);
					}
					user.addRating();
					usuarios.agregarElementoPrincipio(user);
				}
				VOTag voR = new VOTag();

				voR.setIdPelicula(movieID);
				voR.setIdUsuario(userID);
				voR.setTag(tag);
				voR.setTimestamp(timeStamp);

				tageadas.agregarElementoFinal(voR);
				
				boolean encontrada=false;
				Iterator<VOPelicula> pato=pelis.iterator();
				while(pato.hasNext()&&!encontrada)
				{
					VOPelicula madreMia= pato.next();
					if(madreMia.getIdUsuario()==movieID)
					{
						madreMia.setNumeroTags(madreMia.getNumeroTags()+1);
						madreMia.getTagsAsociados().agregarElementoFinal(tag);
						encontrada=true;
					}
				}
				
				linea = in.readLine();
			}
			aha=true;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aha;
	}

	public int sizeMoviesSR() {
		// TODO Auto-generated method stub
		return pelis.darNumeroElementos();
	}

	public int sizeUsersSR() {
		// TODO Auto-generated method stub
		return usuarios.darNumeroElementos();
	}

	public int sizeTagsSR() {
		// TODO Auto-generated method stub
		return tageadas.darNumeroElementos();
	}

	public ILista<VOGeneroPelicula> peliculasPopularesSR(int n) {
		// TODO Auto-generated method stub 
		ordenarPelisPorRating(pelis,0,pelis.darNumeroElementos()-1);
		ILista<VOGeneroPelicula> result=ogeneros;
		Iterator<VOGeneroPelicula> iter=result.iterator();
		while(iter.hasNext())
		{
			VOGeneroPelicula damn= iter.next();
			setUpPeliculasPopulares(damn.getPeliculas());
			ListaEncadenada<VOPelicula> gg=(ListaEncadenada<VOPelicula>)damn.getPeliculas();
			ordenarPelisPorRating(gg,0,gg.darNumeroElementos());
			damn.setPeliculas(gg);
			ListaEncadenada<VOPelicula> tamanioN=new ListaEncadenada<VOPelicula>();
			Iterator<VOPelicula> iter2=gg.iterator();
			int num=n;
			while(iter2.hasNext()&&num!=0)
			{
				tamanioN.agregarElementoFinal(iter2.next());
				num--;
			}
			damn.setPeliculas(tamanioN);
		}
		return result;
	}

	public ILista<VOPelicula> setUpPeliculasPopulares(ILista<VOPelicula> iLista)
	{
		Iterator<VOPelicula> killMe = pelis.iterator();
		boolean cestfinni=false;
		Iterator<VOPelicula> killMe2 = iLista.iterator();
		while(killMe2.hasNext()&&!cestfinni)
		{
			VOPelicula madreMia= killMe2.next();
			while(killMe.hasNext()&&!cestfinni)
			{
				VOPelicula damn= killMe.next();
				if(damn.getTitulo().equals(madreMia.getTitulo()))
				{
					madreMia=damn;
					cestfinni=true;
				}
			}
		}
		return iLista;
	}
	public ILista<VOPelicula> catalogoPeliculasOrdenadoSR() {
		// TODO Auto-generated method stub
		ListaEncadenada<VOPelicula> ret= pelis;
		QuickSort<VOPelicula> quick=new QuickSort<>(ret, 0, pelis.darNumeroElementos()-1);
		return ret;
	}

	public ILista<VOGeneroPelicula> recomendarGeneroSR() {
		// TODO Auto-generated method stub
		ordenarPelisCalificacion(pelis,0,pelis.darNumeroElementos()-1);
		ILista<VOGeneroPelicula> result=ogeneros;
		Iterator<VOGeneroPelicula> iter=result.iterator();
		while(iter.hasNext())
		{
			VOGeneroPelicula damn= iter.next();
			setUpPeliculasPopulares(damn.getPeliculas());
			ListaEncadenada<VOPelicula> gg=(ListaEncadenada<VOPelicula>)damn.getPeliculas();
			ordenarPelisCalificacion(gg,0,gg.darNumeroElementos());
			damn.setPeliculas(gg);
			ListaEncadenada<VOPelicula> tamanioN=new ListaEncadenada<VOPelicula>();
			Iterator<VOPelicula> iter2=gg.iterator();
			while(iter2.hasNext())
			{
				tamanioN.agregarElementoFinal(iter2.next());
			}
			damn.setPeliculas(tamanioN);
		}
		return result;
	}
	public ILista<VOPelicula> setUpRecomendarGenero(ILista<VOPelicula> iLista)
	{
		Iterator<VOPelicula> killMe = pelis.iterator();
		boolean cestfinni=false;
		Iterator<VOPelicula> killMe2 = iLista.iterator();
		while(killMe2.hasNext()&&!cestfinni)
		{
			VOPelicula madreMia= killMe2.next();
			while(killMe.hasNext()&&!cestfinni)
			{
				VOPelicula damn= killMe.next();
				if(damn.getTitulo().equals(madreMia.getTitulo()))
				{
					madreMia=damn;
					cestfinni=true;
				}
			}
		}
		return iLista;
	}

	public ILista<VOGeneroPelicula> opinionRatingsGeneroSR() {
		// TODO Auto-generated method stub
		if(tageadas.darNumeroElementos()==0)
			return null;
		else
		{
		QuickSort<VOPelicula> quick=new QuickSort<>(pelis,0,pelis.darNumeroElementos()-1);
		ILista<VOGeneroPelicula> result=ogeneros;
		Iterator<VOGeneroPelicula> iter=result.iterator();
		while(iter.hasNext())
		{
			VOGeneroPelicula damn= iter.next();
			setUpPeliculasPopulares(damn.getPeliculas());
			ListaEncadenada<VOPelicula> gg=(ListaEncadenada<VOPelicula>)damn.getPeliculas();
			QuickSort<VOPelicula> quick1=new QuickSort<>(gg,0,gg.darNumeroElementos());
			damn.setPeliculas(gg);
			ListaEncadenada<VOPelicula> tamanioN=new ListaEncadenada<VOPelicula>();
			Iterator<VOPelicula> iter2=gg.iterator();
			while(iter2.hasNext())
			{
				tamanioN.agregarElementoFinal(iter2.next());
			}
			damn.setPeliculas(tamanioN);
		}
		return result;
		}

	}

	public ILista<VOPeliculaPelicula> recomendarPeliculasSR(String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		ListaEncadenada<VOPeliculaPelicula> aha=new ListaEncadenada<VOPeliculaPelicula>();
		try (BufferedReader in=new BufferedReader(new FileReader(new File(rutaRecomendacion))))
		{
			Queue<String[]> recomendacion= new Queue<String[]>();
			String linea=in.readLine();
			while(linea!=null&&n!=0)
			{
				String args[]=linea.split(",");
				int movieID=Integer.parseInt(args[0]);
				double rating=Double.parseDouble(args[1]);
				recomendacion.enqueue(args);
				Iterator<VOPelicula> iter=pelis.iterator();
				while(iter.hasNext())
				{
					VOPelicula x=iter.next();
					if(x.getIdUsuario()==movieID)
					{
						VOPeliculaPelicula a= new VOPeliculaPelicula();
						a.setPelicula(x);
						ListaEncadenada<VOPelicula> recom= new ListaEncadenada<VOPelicula>();
						String gen=x.getGenerosAsociados().darElemento(0);
						Iterator<VOGeneroPelicula> iter2=ogeneros.iterator();
						while(iter2.hasNext())
						{
							VOGeneroPelicula y=iter2.next();
							if(gen.equals(y.getGenero()))
							{
								ordenarPelisCalificacion((ListaEncadenada<VOPelicula>) y.getPeliculas(), 0, y.getPeliculas().darNumeroElementos()-1);
								Iterator<VOPelicula> iter3=(Iterator<VOPelicula>)y.getPeliculas().iterator();
								while(iter3.hasNext())
								{
									VOPelicula z=iter3.next();
									double difer= rating-z.getPromedioRatings();
									if(difer>0)
									difer*=-1;
									if(difer<=0.5)
										recom.agregarElementoFinal(z);
								}
							}
						}
						ordenarPelisPorDiferCalif(recom, 0, recom.darNumeroElementos()-1, rating);
						a.setPeliculasRelacionadas(recom);
						aha.agregarElementoFinal(a);
					}
				}
				linea = in.readLine();
			}
			
		} 
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return aha;
	}

	public ILista<VORating> ratingsPeliculaSR(long idPelicula) {
		// TODO Auto-generated method stub
		ListaEncadenada<VORating> result= new ListaEncadenada<VORating>();
		if(rateadas.darNumeroElementos()!=0)
		{
			Iterator<VORating> pel= rateadas.iterator();
			while(pel.hasNext())
			{
				VORating esta=pel.next();
				if(idPelicula==esta.getIdPelicula())
				{
					result.agregarElementoPrincipio(esta);
				}
			}
		}
		QuickSort<VORating> quick = new QuickSort<>(result, 0, result.darNumeroElementos()-1);
		return result;
	}

	//
	//
	//-----------------------------------------------------Parte2
	//
	//
	/**
	 * pre:se asume que se cargo la lista de VOGeneroUsuario
	 */
	public ILista<VOGeneroUsuario> usuariosActivosSR(int n) {
		// TODO Auto-generated method stub
		ListaEncadenada<VOGeneroUsuario> listaFinalPutos=new ListaEncadenada<>();
		Iterator<VOGeneroUsuario> geIterator=usuariosNumRatingsGenero.iterator();
		int i=0;
		while(geIterator.hasNext() &&i<n)
		{
			listaFinalPutos.agregarElementoFinal(geIterator.next());
			i++;
		}
		return listaFinalPutos;		
	}
	public void setupListGeneroUsuario()
	{
		ListaEncadenada<VOGeneroUsuario> genUs= new ListaEncadenada<VOGeneroUsuario>();
		Iterator<String> iterate=nomGeneros.iterator();
		while(iterate.hasNext())
		{
			VOGeneroUsuario gU=new VOGeneroUsuario();
			ListaEncadenada<VOUsuarioConteo> usu=new ListaEncadenada<VOUsuarioConteo>();
			gU.setGenero(iterate.next());
			gU.setUsuarios(usu);
			genUs.agregarElementoPrincipio(gU);			
		}
		Iterator<VORating> iter=rateadas.iterator();
		VORating rating=iter.next();
		ListaEncadenada<String> generosUsuario=new ListaEncadenada<>();
		while(iter.hasNext())
		{
			VOPelicula peli=buscarId(rating.getIdPelicula());
			Iterator<String>generosPeli=peli.getGenerosAsociados().iterator();
			while(generosPeli.hasNext())
			{
				String gen=generosPeli.next();
				if(!(generosUsuario.existeElemento(gen)))
				{
					generosUsuario.agregarElementoPrincipio(gen);
				}
			}
			long id=rating.getIdUsuario();
			int contador=1;
			rating=iter.next();
			contador++;
			if(id!=rating.getIdUsuario())
			{	
				VOUsuarioConteo user=new VOUsuarioConteo();
				user.setConteo(contador);
				user.setIdUsuario(id);
				Iterator<VOGeneroUsuario> duck=genUs.iterator();
				Iterator<String> geIterator=generosUsuario.iterator();
				while(geIterator.hasNext())
				{
					String genActual=geIterator.next();
					while(duck.hasNext())
					{
						VOGeneroUsuario mad=duck.next();
						if(mad.getGenero().equals(genActual))
						{
							mad.concatForList(user);
						}
					}
				}
				contador=0;
				generosUsuario=new ListaEncadenada<>();
			}
		}
		usuariosNumRatingsGenero=genUs;
		Iterator<VOGeneroUsuario> almostLasIter=usuariosNumRatingsGenero.iterator();
		while(almostLasIter.hasNext())
		{
			VOGeneroUsuario genAndUs=almostLasIter.next();
			ListaEncadenada<VOUsuarioConteo> listaFinal=(ListaEncadenada<VOUsuarioConteo>) genAndUs.getUsuarios();
			QuickSort<VOUsuarioConteo> quick=new QuickSort<>(listaFinal, 0, listaFinal.darNumeroElementos()-1);
		}
	}
	public ILista<VOUsuario> catalogoUsuariosOrdenadoSR() {
		// TODO Auto-generated method stub
		QuickSort<VOUsuario> quick=new QuickSort<>(usuarios, 0, usuarios.darNumeroElementos()-1);
		return usuarios;
	}

	public ILista<VOGeneroPelicula> recomendarTagsGeneroSR(int n) {
		// TODO Auto-generated method stub
	   ListaEncadenada<VOGeneroPelicula> lista=new ListaEncadenada<>();
	   Iterator<VOGeneroPelicula> generosYpelis=ogeneros.iterator();
	   while(generosYpelis.hasNext())
	   {
		   VOGeneroPelicula genPelis=generosYpelis.next();
		   ListaEncadenada<VOPelicula> list=(ListaEncadenada<VOPelicula>) genPelis.getPeliculas();
		   ordenarPelisPorTags(list,0,list.darNumeroElementos()-1);
		   Iterator<VOPelicula> iter=list.iterator();
		   int num=0;
		   while(iter.hasNext()&&num<=n)
		   {
			   VOGeneroPelicula resultado=new VOGeneroPelicula();
			   resultado.setGenero(genPelis.getGenero());
			   resultado.setPeliculas(list);
			   lista.agregarElementoPrincipio(resultado);
			   num++;
		   }
	   }
	   return lista;
	}

	public ILista<VOUsuarioGenero> opinionTagsGeneroSR() {
		// TODO Auto-generated method stub
		ListaEncadenada<VOUsuarioGenero> usuGen=new ListaEncadenada<>();
		ListaEncadenada<VOGeneroTag> generosTag=new ListaEncadenada<>();
		Iterator<String> generos=nomGeneros.iterator();
		while(generos.hasNext())
		{
			VOGeneroTag genTa=new VOGeneroTag();
			genTa.setGenero(generos.next());
		}
		Iterator<VOTag> iter=tageadas.iterator();
		if(tageadas.darNumeroElementos()!=0)
		{
			VOTag tag=iter.next();
			while(iter.hasNext())
			{
				VOPelicula pelicula= buscarId(tag.getIdPelicula());
				ListaEncadenada<String> genAsoci=(ListaEncadenada<String>) pelicula.getGenerosAsociados();
				Iterator<String> iterator=genAsoci.iterator();
				Iterator<VOGeneroTag> masIter=generosTag.iterator();
				ListaEncadenada<VOGeneroTag> tagsporUsario=new ListaEncadenada<>();
				while(masIter.hasNext())
				{
					VOGeneroTag genActual=masIter.next();
					while(iterator.hasNext())
					{
						String genero=iterator.next();
						if(genero.equals(genActual.getGenero()))
						{
							genActual.addTag(tag.getTag());
							tagsporUsario.agregarElementoPrincipio(genActual);
						}
					}
				}
				Iterator<VOGeneroTag> ultiIter=tagsporUsario.iterator();
				while(ultiIter.hasNext())
				{
					VOGeneroTag generoYTag=ultiIter.next();
					ListaEncadenada<String> misTags=(ListaEncadenada<String>) generoYTag.getTags();
					QuickSort<String> quick=new QuickSort<>(misTags, 0, misTags.darNumeroElementos()-1);
				}
				long id=tag.getIdUsuario();
				tag=iter.next();
				if(id!=tag.getIdUsuario())
				{
					VOUsuarioGenero usu=new VOUsuarioGenero();
					usu.setIdUsuario(id);
					usu.setListaGeneroTags(tagsporUsario);
					usuGen.agregarElementoFinal(usu);
					tagsporUsario=new ListaEncadenada<>();
				}
			}
		}
		return usuGen;
	}

	public ILista<VOPeliculaUsuario> recomendarUsuariosSR(
			String rutaRecomendacion, int n) {
		// TODO Auto-generated method stub
		return null;
	}

	public ILista<VOTag> tagsPeliculaSR(int idPelicula) {
		// TODO Auto-generated method stub	
		QuickSort<VOTag> quick=new QuickSort<>(tageadas, 0, tageadas.darNumeroElementos()-1);
		return tageadas;
	}

	public void agregarOperacionSR(String nomOperacion, long tinicio, long tfin) {
		// TODO Auto-generated method stub
		VOOperacion a= new VOOperacion();
		a.setOperacion(nomOperacion);
		a.setTimestampInicio(tinicio);
		a.setTimestampFin(tfin);
		operaciones.push(a);
	}

	public ILista<VOOperacion> darHistoralOperacionesSR() {
		// TODO Auto-generated method stub
		Stack<VOOperacion> copia=operaciones;
		ListaEncadenada<VOOperacion> result=new ListaEncadenada<VOOperacion>();;
		while(!copia.isEmpty())
		{
			result.agregarElementoFinal(copia.pop());
		}
		return result;
	}

	public void limpiarHistorialOperacionesSR() {
		// TODO Auto-generated method stub
		while(!operaciones.isEmpty())
		{
			operaciones.pop();
		}
	}

	public ILista<VOOperacion> darUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		Stack<VOOperacion> copia=operaciones;
		ListaEncadenada<VOOperacion> result=new ListaEncadenada<VOOperacion>();;
		while(!copia.isEmpty()&&n!=0)
		{
			result.agregarElementoFinal(copia.pop());
			n--;
		}
		return result;
	}

	public void borrarUltimasOperaciones(int n) {
		// TODO Auto-generated method stub
		while(!operaciones.isEmpty()&&n!=0)
		{
			operaciones.pop();
			n--;
		}
	}

	public long agregarPelicula(String titulo, int agno, String[] generos) {
		// TODO Auto-generated method stub
		return 0;
	}

	public void agregarRating(int idUsuario, int idPelicula, double rating) {
		// TODO Auto-generated method stub
		
	}
	public VOPelicula buscarId(long id)
	{
	    Iterator<VOPelicula> iter=pelis.iterator();
	    while(iter.hasNext())
	    {
	    	VOPelicula peli=iter.next();
	    	if(peli.getIdUsuario()==id)
	    	{
	    		return peli;
	    	}
	    }
	    return null;
	}
}
