package model.vo;

public class VOTag implements Comparable<VOTag>{
	private long idUsuario;
	private long idPelicula;
	private String tag;
	private long timestamp;

	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getIdPelicula() {
		return idPelicula;
	}
	public void setIdPelicula(long idPelicula) {
		this.idPelicula = idPelicula;
	}
	public String getTag() {
		return tag;
	}
	public void setTag(String tag) {
		this.tag = tag;
	}
	public long getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}
	@Override
	public int compareTo(VOTag o) {
		if(timestamp==o.getTimestamp())
		{
			return 0;
		}
		else if(timestamp>o.getTimestamp())
		{
			return -1;
		}
		else
		{
			return 1;
		}

	}

}
