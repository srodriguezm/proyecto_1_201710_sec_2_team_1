package model.vo;

public class VOUsuario implements Comparable<VOUsuario>{

	private long idUsuario;	
	private long primerTimestamp;
	private int numRatings;
	private double diferenciaOpinion;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public long getPrimerTimestamp() {
		return primerTimestamp;
	}
	public void setPrimerTimestamp(long primerTimestamp) {
		this.primerTimestamp = primerTimestamp;
	}
	public int getNumRatings() {
		return numRatings;
	}
	public void setNumRatings(int numRatings) {
		this.numRatings = numRatings;
	}
	public void addRating()
	{
		numRatings++;
	}
	public double getDiferenciaOpinion() {
		return diferenciaOpinion;
	}
	public void setDiferenciaOpinion(double diferenciaOpinion) {
		this.diferenciaOpinion = diferenciaOpinion;
	}
	@Override
	public int compareTo(VOUsuario o) {
		if(primerTimestamp==o.getPrimerTimestamp())
		{
			if(numRatings==o.getNumRatings())
			{
				if(idUsuario==o.getIdUsuario())
				{
					return 0;
				}
				else if(idUsuario<o.getIdUsuario())
				{
					return -1;
				}
				else
				{
					return 1;
				}
			}
			else if(numRatings<o.getNumRatings())
			{
				return -1;
			}
			else
			{
				return 1;
			}
		}
		else if(primerTimestamp<o.getPrimerTimestamp())
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
	
}
