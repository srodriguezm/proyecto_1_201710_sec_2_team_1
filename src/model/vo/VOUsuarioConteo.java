package model.vo;

import model.data_structures.ILista;

public class VOUsuarioConteo implements Comparable<VOUsuarioConteo>{
	private long idUsuario;
	private int conteo;
	public long getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(long idUsuario) {
		this.idUsuario = idUsuario;
	}
	public int getConteo() {
		return conteo;
	}
	public void setConteo(int conteo) {
		this.conteo = conteo;
	}
	@Override
	public int compareTo(VOUsuarioConteo o) {
		// TODO Auto-generated method stub
		if(conteo==o.getConteo())
		{
			return 0;
		}
		else if(conteo>o.getConteo())
		{
			return -1;
		}
		else
		{
			return 1;
		}
	}
}
